import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserServices } from '../user/user.service';

@Injectable()
export class AuthService {
  constructor(
      private usersService: UserServices,
      private jwtService: JwtService
    ) {}

  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.usersService.findOne(username);
    if (user && user.password === pass) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }

  async login(user: any) {
    const payload = { username: user.name, sub: user.id };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}