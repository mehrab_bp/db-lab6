import { Body, Controller, Get, Header, ParseIntPipe, Post, Put, Query, Req, UseGuards, Request } from '@nestjs/common';
import { UserServices } from './user.service';
import CreateUserDto from './dto/create-user.dto';
import { ApiBearerAuth } from '@nestjs/swagger';
import { runInThisContext } from 'vm';
import { AuthGuard } from '@nestjs/passport';
import { JwtAuthGuard } from 'src/auth/JwtAuthGuard';

@Controller('users')
export class UserController {
  constructor(private readonly usersServices: UserServices) {}

//'postUser()' will handle the creating of new User
  @Post('post')
  postUser( @Body() user: CreateUserDto) {
    return this.usersServices.insert(user);
  }

// 'getAll()' returns the list of all the existing users in the database
  @Get()
  getAll() {
    return this.usersServices.getAllUsers();
  }

//'getBooks()' return all the books which are associated with the user 
// provided through 'userID' by the request  
  @Header('Content-Type', 'application/json')
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Get('books')
  getBooks( @Request() req) {
    const user = req.user;
    console.log('user :', user);
    return this.usersServices.getBooksOfUser(user.userId);
  }
}