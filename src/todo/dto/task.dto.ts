export class TaskDTO {
    name: string;
    userID: number;
    categoryID: number;
    tagIDs: number[];
    itemIDs: number[];
    description: string;
}