import { Injectable, MethodNotAllowedException, NotFoundException } from '@nestjs/common';
import CategoryEntity from 'src/db/category.entity';
import ItemEntity from 'src/db/item.entity';
import TagEntity from 'src/db/tag.entity';
import TaskEntity from 'src/db/task.entity';
import UserEntity from 'src/db/user.entity';
import { UserServices } from 'src/user/user.service';
import { CategoryDTO } from './dto/category.dto';
import { ItemDTO } from './dto/item.dto';
import { TagDTO } from './dto/tag.dto';
import { TaskDTO } from './dto/task.dto';

@Injectable()
export class TodoService {
    async insertTag(tagDetail: TagDTO): Promise<TagEntity> {
        const {name} = tagDetail;
        const tag = new TagEntity();
        tag.name = name;
        await tag.save();
        return tag;
    }

    async insertCategory(categDetail: CategoryDTO): Promise<CategoryEntity> {
        const {name} = categDetail;
        const category = new CategoryEntity();
        category.name = name;
        await category.save();
        return category;
    }

    async insertItem(itemDetail: ItemDTO): Promise<ItemEntity> {
        const {workDesc} = itemDetail;
        const item = new ItemEntity();
        item.workDescription = workDesc;
        await item.save();
        return item;
    }

    async insertTask(taskDetail: TaskDTO): Promise<TaskEntity> {
        const {name, userID, categoryID, tagIDs, itemIDs, description} = taskDetail;
        const task = new TaskEntity();
        task.name = name;
        task.description = description;
        task.user = await UserEntity.findOne(userID);
        if (task.user === null || task.user === undefined) {
            throw new NotFoundException();
        }
        task.category = await CategoryEntity.findOne(categoryID);
        task.items = [];
        for (let i = 0; i < itemIDs.length; i++) {
            const item = await ItemEntity.findOne(itemIDs[i]);
            task.items.push(item);
        }

        task.tags = [];
        for (let i = 0; i < tagIDs.length; i++) {
            const tag = await TagEntity.findOne(tagIDs[i]);
            task.tags.push(tag);
        }
        await task.save();
        return task;
    }

    async removeItem(id: number, userId: number): Promise<ItemEntity> {
        let item = await ItemEntity.findOne(id);

        if (item === null || item === undefined) {
            throw new NotFoundException();
        }
        const task = await item.task;
        if (task.user.id !== userId) {
            throw new MethodNotAllowedException();
        }

        item = await item.remove();
        return item;
    }

    async removeTask(id: number, userId: number): Promise<TaskEntity> {
        let task = await TaskEntity.findOne(id);

        if (task === null || task === undefined) {
            throw new NotFoundException();
        }
        if (task.user.id !== userId) {
            throw new MethodNotAllowedException();
        }

        await task.category.remove();
        task = await task.remove();
        return task;
    }

    async editTask(id: number, {name, userID, categoryID, tagIDs, itemIDs}: TaskDTO, userId: number) : Promise<TaskEntity> {
        let task = await TaskEntity.findOne(id);

        if (task === null || task === undefined) {
            throw new NotFoundException();
        } 
        if (task.user.id !== userId) {
            throw new MethodNotAllowedException();
        }

        task.name = name;
        task.user = await UserEntity.findOne(userID);
        if (task.user === null || task.user === undefined) {
            throw new NotFoundException();
        }
        task.category = await CategoryEntity.findOne(categoryID);
        task.items = [];
        for (let i = 0; i < itemIDs.length; i++) {
            const item = await ItemEntity.findOne(itemIDs[i]);
            task.items.push(item);
        }

        task.tags = [];
        for (let i = 0; i < tagIDs.length; i++) {
            const tag = await TagEntity.findOne(tagIDs[i]);
            task.tags.push(tag);
        }
        await TaskEntity.save(task);
        return task;
    }

    async getTasks(userId: number) {
        const user = await UserEntity.findOne(userId);
        if (user === null || user === undefined) {
            throw new NotFoundException();
        }
        return await TaskEntity.find({where: {user: user}, relations: ['category', 'tags', 'items']})
    }
}
