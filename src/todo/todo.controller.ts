import { Body, Controller, Delete, Header, Param, Post, Put, UseGuards, Request, Get } from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/JwtAuthGuard';
import ItemEntity from 'src/db/item.entity';
import TagEntity from 'src/db/tag.entity';
import TaskEntity from 'src/db/task.entity';
import { CategoryDTO } from './dto/category.dto';
import { ItemDTO } from './dto/item.dto';
import { TagDTO } from './dto/tag.dto';
import { TaskDTO } from './dto/task.dto';
import { TodoService } from './todo.service';

@Controller('todo')
export class TodoController {

    constructor(private readonly todoService: TodoService) {}

    @Header('Content-Type', 'application/json')
    @ApiBearerAuth()
    @UseGuards(JwtAuthGuard)
    @Post('postTag')
    async postTag(@Body() tag: TagDTO): Promise<TagEntity> {
        return this.todoService.insertTag(tag);
    }

    @Header('Content-Type', 'application/json')
    @ApiBearerAuth()
    @UseGuards(JwtAuthGuard)
    @Post('postCategory')
    async postCateg(@Body() categ: CategoryDTO): Promise<CategoryDTO> {
        return this.todoService.insertCategory(categ);
    }

    @Header('Content-Type', 'application/json')
    @ApiBearerAuth()
    @UseGuards(JwtAuthGuard)
    @Post('postItem')
    async postItem(@Body() item: ItemDTO): Promise<ItemEntity> {
        return this.todoService.insertItem(item);
    }

    @Header('Content-Type', 'application/json')
    @ApiBearerAuth()
    @UseGuards(JwtAuthGuard)
    @Post('postTask')
    async postTask( @Body() task: TaskDTO): Promise<TaskEntity> {
        console.log(task)
        return this.todoService.insertTask(task);
    }

    @Header('Content-Type', 'application/json')
    @ApiBearerAuth()
    @UseGuards(JwtAuthGuard)
    @Put('task/:id')
    async editTask(@Param('id') id: number, @Body() task: TaskDTO, @Request() req): Promise<TaskEntity> {
        return this.todoService.editTask(id, task, req.user.userId);
    }

    @Header('Content-Type', 'application/json')
    @ApiBearerAuth()
    @UseGuards(JwtAuthGuard)
    @Delete('task/:id')
    async deleteTask(@Param('id') id: number, @Request() req): Promise<TaskEntity> {
        return this.todoService.removeTask(id, req.user.userId);
    }

    @Header('Content-Type', 'application/json')
    @ApiBearerAuth()
    @UseGuards(JwtAuthGuard)
    @Delete('item/:id')
    async deleteItem(@Param('id') id: number, @Request() request): Promise<ItemEntity> {
        return this.todoService.removeItem(id, request.user.userId);
    }

    @Header('Content-Type', 'application/json')
    @ApiBearerAuth()
    @UseGuards(JwtAuthGuard)
    @Get('tasks')
    async getTasks(@Request() req) {
        return this.todoService.getTasks(req.user.userId);
    }
}
