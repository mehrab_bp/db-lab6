import { Body, Controller, Get, Post, Put, Delete, Param } from '@nestjs/common';
import { BooksService } from './books.service';
import CreateBookDto from './dto/create-book.dto'

@Controller('books')
export class BooksController {
    constructor(private readonly booksService: BooksService) {}

    @Post('post')
    postBook( @Body() book: CreateBookDto) {
        return this.booksService.insert(book);
    }

    @Get()
    getAll() {
        return this.booksService.getAllBooks();
    }

    @Put(':id')
    editbook( @Param('id') id: number, @Body() book: CreateBookDto) {
        return this.booksService.edit(id, book);
    }

    @Delete(':id')
    removeBook(@Param('id') id: number) {
        return this.booksService.remove(id);
    }
}
