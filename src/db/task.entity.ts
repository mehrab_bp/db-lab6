import { type } from 'os';
import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToMany, OneToOne, ManyToOne, ManyToMany, JoinColumn, JoinTable } from 'typeorm';
import CategoryEntity from './category.entity';
import ItemEntity from './item.entity';
import TagEntity from './tag.entity';
import UserEntity from './user.entity';

@Entity()
export default class TaskEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column({ length: 800 })
    description: string;

    @OneToMany(type => ItemEntity , item => item.task)
    items: ItemEntity[];

    @OneToOne(type => CategoryEntity)
    @JoinColumn()
    category: CategoryEntity;
    // @Column()
    // categoryId: number;

    @ManyToMany(type => TagEntity)
    @JoinTable()
    tags: TagEntity[];

    @ManyToOne(type => UserEntity , user => user.tasks, {
        eager: true
    })
    user: UserEntity;

}